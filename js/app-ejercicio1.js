// OBTENEMOS LOS BOTONES CON LOS QUE TRABAJAREMOS
const btnCalcular = document.getElementById('btnCalcular');
const btnLimpiar = document.getElementById('btnLimpiar');
const btnEnviar = document.getElementById('btnEnviar');

// CODIFICAMOS EL BOTON CALCULAR
btnCalcular.addEventListener('click', function(){
    let precio = document.getElementById('txtPrecio').value;
    let tipoViaje = document.getElementById('tipoViaje').value;
    let subtotal = 0;
    let impuesto = 0;
    let total = 0;

    // CALCULAMOS EL SUBTOTAL
    if(tipoViaje == 1){
        subtotal= (precio * 1);
        impuesto = (subtotal * 0.16);
        total = subtotal + impuesto;
    }else if(tipoViaje == 2){
        subtotal = (precio * 1.8);
        impuesto = (subtotal * 0.16);
        total = subtotal + impuesto;
    }


    // MOSTRAMOS LOS DATOS
    document.getElementById('txtSubtotal').value = subtotal.toFixed(2);
    document.getElementById('txtImpuesto').value = impuesto.toFixed(2);
    document.getElementById('txtTotalPago').value = total.toFixed(2);

});

// CODIFICAMOS EL BOTON LIMPIAR
btnLimpiar.addEventListener('click', function(){
    document.getElementById('txtnumBoleto').value = '';
    document.getElementById('txtnomCliente').value = '';
    document.getElementById('txtDestino').value = '';
    document.getElementById('txtPrecio').value = '';
    document.getElementById('txtSubtotal').value = '';
    document.getElementById('txtImpuesto').value = '';
    document.getElementById('txtTotalPago').value = '';
});

// CODIFICAMOS EL BOTON ENVIAR
btnEnviar.addEventListener('click', function(){

});