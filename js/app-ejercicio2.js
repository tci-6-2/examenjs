// OBTENEMOS LOS BOTONES CON LOS QUE TRABAJAREMOS
const btnCalcular = document.getElementById('btnCalcular');
const btnLimpiar = document.getElementById('btnLimpiar');


// CODIFICAMOS EL BOTON CALCULAE
btnCalcular.addEventListener('click', function () {
    calGrados();
});

function calGrados() {

    const cant = parseFloat(document.getElementById('txtCantidad').value);
    const resultado = document.getElementById('res');

    var tipo = document.querySelector('input[name="convertidor"]:checked').value;
    if (tipo === "celsius") {
        const fahrenheit = (cant * 9 / 5) + 32; // FORMULA
        resultado.textContent = " Son " + fahrenheit + "° Fahrenheit.";
    } else if (tipo === "fahrenheit") {
        const celsius = (cant - 32) * 5 / 9; // FORMULA
        resultado.textContent = "Son " + celsius + "° Celsius.";
    } else {
        resultado.textContent = "Ingrese una opcion valida";
    };
}

btnLimpiar.addEventListener('click', function () {
    document.getElementById('txtCantidad').value = '';
    document.getElementById('res').textContent = ''; 
});

function deseleccionarRadio() {
    var convertidor = document.getElementsByName('convertidor');

    for (var i = 0; i < convertidor.length; i++) {
        convertidor[i].checked = false;
    }
}