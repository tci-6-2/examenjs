// OBTENEMOS LOS BOTONES CON LOS QUE TRABAJAREMOS
const btnGenerar = document.getElementById('btnGenerar');
const btnLimpiar = document.getElementById('btnLimpiar');

// CODIFICAMOS LOS BOTONES
btnGenerar.addEventListener("click", function () {
    generarnumeros();
});

btnLimpiar.addEventListener("click", function () {
    document.getElementById("txtEdades").value = "";
    document.getElementById("bebe").textContent = "0";
    document.getElementById("nino").textContent = "0";
    document.getElementById("adolecente").textContent = "0";
    document.getElementById("adulto").textContent = "0";
    document.getElementById("ancianos").textContent = "0";
    document.getElementById("edadprom").textContent = "0";
});

// FUNCIONES
// ... (Tu código existente)

// FUNCIONES
function generarnumeros() {
    var txtEdades = document.getElementById("txtEdades");
    var numeros = [];
    for (var i = 0; i < 100; i++) {
        var numerosRandom = Math.floor(Math.random() * 100) + 1;
        numeros.push(numerosRandom);
    }
    var numerosTexto = numeros.join(", ");
    txtEdades.value = numerosTexto;

    var bebecontador = 0;
    var ninocontador = 0;
    var adolecentecontador = 0;
    var adultocontador = 0;
    var ancianocontador = 0;
    var totalEdades = 0; // Variable para almacenar la suma de edades

    for (var i = 0; i < numeros.length; i++) {
        var edad = numeros[i];
        totalEdades += edad; // Suma de todas las edades

        if (edad >= 1 && edad <= 3) {
            bebecontador++;
        } else if (edad >= 4 && edad <= 12) {
            ninocontador++;
        } else if (edad >= 13 && edad <= 17) {
            adolecentecontador++;
        } else if (edad >= 18 && edad <= 60) {
            adultocontador++;
        } else {
            ancianocontador++;
        }
    }

    // Cálculo del promedio
    var promedio = totalEdades / numeros.length;

    // Mostrar el promedio en la consola
    document.getElementById('edadprom').textContent = "Edad promedio: " + promedio;

    // Resto de tu código para mostrar conteos por categoría
    document.getElementById("bebe").textContent = bebecontador;
    document.getElementById("nino").textContent = ninocontador;
    document.getElementById("adolecente").textContent = adolecentecontador;
    document.getElementById("adulto").textContent = adultocontador;
    document.getElementById("ancianos").textContent = ancianocontador;
}
